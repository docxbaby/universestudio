﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace UniverseStudio
{
    internal class TCPServer
    {
        static string ipAddress = "192.168.100.5"; // IP address of the server
        static int port = 9999; // Port number to listen on
        TcpListener server = new TcpListener(IPAddress.Parse(ipAddress), port);
        public void Start(RichTextBox output)
        {
            Console.WriteLine("Iniciando el servidor");
         
            try {

                server = new TcpListener(IPAddress.Parse(ipAddress), port);
                server.Start();
                Console.WriteLine("Server started. Listening for incoming connections...");
            }
            catch (Exception e) {
                Console.WriteLine("Ocurrio un error al iniciar el servidor TCP");
                Console.WriteLine(e.ToString());
            }
            
            output.Text += "Server started. Listening for incoming connections...";

            Thread listenThread = new Thread(() =>
            {
                Console.WriteLine("en el nuevo thread");
                while (true)
                {
                    // Accept an incoming client connection
                    TcpClient client = server.AcceptTcpClient();
                    Console.WriteLine("Client connected.");

                    if (client != null) { 
                        string clientIP = (client.Client.RemoteEndPoint as IPEndPoint).Address.ToString();
                    }


                    // Handle the client connection in a separate thread or task
                    Task.Run(() => HandleClient(client, output));
                }
            });
            
            listenThread.Start();

        }

        private static void HandleClient(TcpClient client, RichTextBox messagesBox)
        {
            // Get the client's network stream for reading and writing
            NetworkStream stream = client.GetStream();

            // Buffer to store the incoming data
            byte[] buffer = new byte[1024];
            int bytesRead;
            Console.WriteLine("Antes de entrar al loop: " );

            while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
            {
                // Convert the received data to a string
                string receivedData = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                //Console.WriteLine("Received data: " + receivedData);
                //messagesBox.Text += "Reciviendo info";
                messagesBox.Text += receivedData;

                // Process the received data as needed
                // You can send a response back to the client using stream.Write()

                // Clear the buffer
                Array.Clear(buffer, 0, buffer.Length);
            }

            // Close the client connection when done
            client.Close();
        }
        
    }

}
