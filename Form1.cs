using System.Net.Sockets;

namespace UniverseStudio
{
    public partial class Form1 : Form
    {
        TCPServer tcpServer;

        public Form1()
        {
            InitializeComponent();

            label2.Text = "Servidor apagado";
            Console.WriteLine("Se abrio la ventana");
            
            try { 

                tcpServer = new TCPServer();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
         }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine ("Click");
            try {
                
               tcpServer.Start(richTextBox1);
                label2.Text = "Iniciando el servidor TCP";
                Console.WriteLine("Iniciando el servidor TCP");

            }
            catch (Exception ex) { 
                Console.WriteLine ("Hubo un error al iniciar el servidor");
                Console.WriteLine (ex.ToString());
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}