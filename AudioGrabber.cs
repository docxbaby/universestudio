﻿using NAudio.CoreAudioApi;
using NAudio.Wave;

namespace UniverseStudio
{
    internal class AudioGrabber
    {
        MMDeviceEnumerator enumerator ;
        MMDevice captureDevice;
        static WaveFormat waveFormat;
        int bufferMilliseconds;
        int bufferSize;
        BufferedWaveProvider bufferedWaveProvider;
        public AudioGrabber() 
        { 
            // Create an audio capture device
            enumerator = new MMDeviceEnumerator();
            captureDevice = enumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
            waveFormat = captureDevice.AudioClient.MixFormat;
            
            // Set up a capture buffer
            bufferMilliseconds = 100; // Adjust the buffer size as per your needs
            bufferSize = (int)(waveFormat.AverageBytesPerSecond / 1000.0 * bufferMilliseconds);
            bufferedWaveProvider = new BufferedWaveProvider(waveFormat);
            bufferedWaveProvider.BufferLength = bufferSize;
        }

        // Event handler for capturing audio
        void DataAvailable(object sender, WaveInEventArgs e)
        {
            bufferedWaveProvider.AddSamples(e.Buffer, 0, e.BytesRecorded);
        }

        public void record() { 
        
            // Create a WaveInEvent to start capturing audio
            WaveInEvent waveInEvent = new WaveInEvent();
            waveInEvent.DeviceNumber = 0; // Use the appropriate audio input device number
            waveInEvent.WaveFormat = waveFormat;
            waveInEvent.DataAvailable += DataAvailable;
            waveInEvent.StartRecording();
        }

        public BufferedWaveProvider GetBufferedWaveProvider()
        {
            return bufferedWaveProvider;
        }
    }

}
